# Number of threads per job
THREADS := 8
GENOME := reference/genome/GRCm38
TRANSCRIPTOME := reference/transcriptome/GRCm38
ANNOTATION := annotation/Mus_musculus.GRCm38.96.gtf

BASE := $(notdir $(patsubst %_1.fastq.gz,%,$(wildcard data/fq/raw/*_1.fastq.gz)))
QC := $(patsubst %,data/qc/%_1_fastqc.zip, $(BASE)) $(patsubst %,data/qc/%_2_fastqc.zip, $(BASE))
ALIGN := $(patsubst %,data/align/genome/%.bam, $(BASE)) $(patsubst %,data/align/transcriptome/%.bam, $(BASE))
COUNT_TX_GTF := $(patsubst %,count/transcriptome/%.gtf.gz, $(BASE))
COUNT_TX := $(patsubst %,count/transcriptome/%.tsv, $(BASE))
COUNT_GX_GTF := $(patsubst %,count/genome/%.gtf.gz, $(BASE))
COUNT_GX := $(patsubst %,count/genome/%.tsv, $(BASE))

$(shell mkdir -p \
	data/fq/preprocess \
	data/qc \
	data/align/genome \
	data/align/transcriptome \
	count/ \
	count/transcriptome \
	count/genome \
)

all: $(QC) $(COUNT_GX) $(COUNT_TX)

data/fq/preprocess/%_1.fastq.gz: data/fq/raw/%_1.fastq.gz
	trimmomatic PE -threads $(THREADS) \
		data/fq/raw/$*_1.fastq.gz data/fq/raw/$*_2.fastq.gz \
		data/fq/preprocess/$*_1.fastq.gz data/fq/preprocess/$*_1U.fastq.gz \
		data/fq/preprocess/$*_2.fastq.gz data/fq/preprocess/$*_2U.fastq.gz \
		LEADING:10 TRAILING:10 SLIDINGWINDOW:4:18 MINLEN:18

data/fq/preprocess/%_2.fastq.gz: data/fq/preprocess/%_1.fastq.gz

data/qc/%_fastqc.zip: data/fq/preprocess/%.fastq.gz
	fastqc -o data/qc $^

data/align/genome/%.bam: data/fq/preprocess/%_1.fastq.gz data/fq/preprocess/%_2.fastq.gz
	hisat2 \
		-p $(THREADS) \
		-x $(GENOME) \
		-1 $< -2 data/fq/preprocess/$*_2.fastq.gz \
		| samtools view -1bS - \
		> $@
	samtools sort \
		-@ $(THREADS) \
		-O bam \
		-T $@.tmp \
		-o $@.sorted \
		$@
	mv $@.sorted $@

data/align/transcriptome/%.bam: data/fq/preprocess/%_1.fastq.gz data/fq/preprocess/%_2.fastq.gz
	hisat2 \
		-p $(THREADS) \
		-x $(TRANSCRIPTOME) \
		-1 $< -2 data/fq/preprocess/$*_2.fastq.gz \
		| samtools view -1bS - \
		> $@
	samtools sort \
		-@ $(THREADS) \
		-O bam \
		-T $@.tmp \
		-o $@.sorted \
		$@
	mv $@.sorted $@


count/transcriptome/%.gtf.gz: data/align/transcriptome/%.bam
	stringtie -G $(ANNOTATION) $^ | gzip > $@

count/transcriptome/%.tsv: count/transcriptome/%.gtf.gz
	zcat $^ \
		| sed 's/; /\t/g' \
		| tr '\t' '\n' \
		| grep reference_id \
		| cut -d" " -f2 \
		| tr -d '"' \
		| sort -k1b,1 \
		| uniq -c \
		| awk 'BEGIN { OFS="\t" } {print $$2, $$1};' \
		| sort -rnk2,2 \
		> $@

count/genome/%.gtf.gz: data/align/genome/%.bam
	stringtie -G $(ANNOTATION) $^ | gzip > $@

count/genome/%.tsv: count/genome/%.gtf.gz
	zcat $^ \
		| sed 's/; /\t/g' \
		| tr '\t' '\n' \
		| grep reference_id \
		| cut -d" " -f2 \
		| tr -d '"' \
		| sort -k1b,1 \
		| uniq -c \
		| awk 'BEGIN { OFS="\t" } {print $$2, $$1};' \
		| sort -rnk2,2 \
		> $@


.PRECIOUS: count/*.gtf
